package io.github.danielm59.infinitetools.gui.inventory;

import org.lwjgl.opengl.GL11;

import io.github.danielm59.infinitetools.InfiniteTools;
import io.github.danielm59.infinitetools.inventory.ContainerIngotEnricher;
import io.github.danielm59.infinitetools.tile.TileIngotEnricher;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;

public class GuiIngotEnricher extends GuiContainer
{
	private TileIngotEnricher tileIngotEnricher;

	public GuiIngotEnricher(InventoryPlayer inventory, TileIngotEnricher IngotEnricher, EntityPlayer player)
	{
		super(new ContainerIngotEnricher(inventory, IngotEnricher, player));
		tileIngotEnricher = IngotEnricher;
		xSize = 176;
		ySize = 166;
	}

	@Override
	protected void drawGuiContainerForegroundLayer(int x, int y)
	{
		String text = I18n.format(tileIngotEnricher.getName());
		int pos = (xSize - fontRendererObj.getStringWidth(text)) / 2;
		fontRendererObj.drawString(text, pos, 6, 0x404040);
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float opacity, int x, int y)
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		this.mc.getTextureManager().bindTexture(new ResourceLocation(InfiniteTools.MODID.toLowerCase(), "textures/gui/2to1Gui.png"));
		int xStart = (width - xSize) / 2;
		int yStart = (height - ySize) / 2;
		this.drawTexturedModalRect(xStart, yStart, 0, 0, xSize, ySize);
		int processPogress = (int) (tileIngotEnricher.getProgress() * 22);
		drawTexturedModalRect(xStart + 80, yStart + 35, 176, 0, processPogress, 15);
	}
}