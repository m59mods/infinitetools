package io.github.danielm59.infinitetools.tile;

import io.github.danielm59.infinitetools.recipe.ingotenricher.IngotEnricherRecipe;
import io.github.danielm59.infinitetools.recipe.ingotenricher.IngotEnricherRegistry;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ITickable;

public class TileIngotEnricher extends TileBase implements ITickable
{
	public int currentProcessTime;
	public TileIngotEnricher()
	{
		super();
		inventory = new ItemStack[3];
	}
	
	@Override
	public String getName() 
	{
		return "Ingot Enricher";
	}

	@Override
	public void update()
	{
		if (!worldObj.isRemote)
		{
			IngotEnricherRecipe recipe = IngotEnricherRegistry.getInstance().getMatchingRecipe(inventory[0], inventory[1], inventory[2]);
			if (recipe != null)
			{
				if (++currentProcessTime >= 100)
				{
					this.markDirty();
					currentProcessTime = 0;
					if (inventory[2] != null)
					{
						inventory[2].stackSize += recipe.getOutput().stackSize;
					} else
					{
						inventory[2] = recipe.getOutput().copy();
					}
					if (inventory[0].getItem().hasContainerItem(inventory[0]))
					{
						setInventorySlotContents(0, inventory[0].getItem().getContainerItem(inventory[0]));
					} else
					{
						decrStackSize(0, recipe.getInputTop().stackSize);
					}
					if (inventory[1].getItem().hasContainerItem(inventory[1]))
					{
						setInventorySlotContents(1, inventory[1].getItem().getContainerItem(inventory[1]));
					} else
					{
						decrStackSize(1, recipe.getInputBottom().stackSize);
					}
				}
			} else
			{
				currentProcessTime = 0;
			}
		}
	}

	public float getProgress()
	{
		return (float) currentProcessTime / 100;
	}
}