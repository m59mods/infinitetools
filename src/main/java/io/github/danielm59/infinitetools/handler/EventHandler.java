package io.github.danielm59.infinitetools.handler;

import org.apache.commons.lang3.ArrayUtils;

import io.github.danielm59.infinitetools.armor.Armor;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.MobEffects;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class EventHandler 
{

	@SubscribeEvent
	public void onLivingUpdateEvent(LivingUpdateEvent event) 
	{
		EntityLivingBase entity = event.getEntityLiving();

		if (entity != null && entity instanceof EntityPlayer) 
		{
			EntityPlayer player = (EntityPlayer) entity;
			ItemStack[] inv = player.inventory.armorInventory;
			if (!ArrayUtils.contains(inv, null))
			{
				ItemStack playerHelmet = player.inventory.armorInventory[3];
				ItemStack playerChest = player.inventory.armorInventory[2];
				ItemStack playerLegs = player.inventory.armorInventory[1];
				ItemStack playerBoots = player.inventory.armorInventory[0];
				if (playerHelmet != null && playerHelmet.getItem().equals(Armor.infHead) && playerChest != null
						&& playerChest.getItem().equals(Armor.infChest) && playerLegs != null
						&& playerLegs.getItem().equals(Armor.infLegs) && playerBoots != null
						&& playerBoots.getItem().equals(Armor.infBoots)) 
				{
					int time = 202;
					player.addPotionEffect(new PotionEffect(MobEffects.RESISTANCE, time, 4, false, false));
					player.addPotionEffect(new PotionEffect(MobEffects.FIRE_RESISTANCE, time, 4, false, false));
					player.addPotionEffect(new PotionEffect(MobEffects.NIGHT_VISION, time, 4, false, false));
					player.addPotionEffect(new PotionEffect(MobEffects.ABSORPTION, time, 4, false, false));
					player.addPotionEffect(new PotionEffect(MobEffects.WATER_BREATHING, time, 4, false, false));
					if (player.capabilities.allowFlying == false) 
					{
						player.capabilities.allowFlying = true;
					}
					return;
				}
			}
			if (player.capabilities.allowFlying == true && !player.capabilities.isCreativeMode) {
				player.capabilities.allowFlying = false;
				player.capabilities.isFlying = false;
			}
		}
	}
}
