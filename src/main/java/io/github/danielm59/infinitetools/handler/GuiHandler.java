package io.github.danielm59.infinitetools.handler;

import io.github.danielm59.infinitetools.gui.inventory.GuiID;
import io.github.danielm59.infinitetools.gui.inventory.GuiIngotEnricher;
import io.github.danielm59.infinitetools.inventory.ContainerIngotEnricher;
import io.github.danielm59.infinitetools.tile.TileIngotEnricher;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;

public class GuiHandler implements IGuiHandler 
{

	@Override
	public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		if (ID == GuiID.INGOT_ENRICHER.ordinal())
		{
			TileIngotEnricher tileIngotEnricher = (TileIngotEnricher) world.getTileEntity(new BlockPos(x, y, z));
			return new ContainerIngotEnricher(player.inventory, tileIngotEnricher, player);
		}
		return null;
		
	}

	@Override
	public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		if (ID == GuiID.INGOT_ENRICHER.ordinal())
		{
			TileIngotEnricher tileIngotEnricher = (TileIngotEnricher) world.getTileEntity(new BlockPos(x, y, z));
			return new GuiIngotEnricher(player.inventory, tileIngotEnricher, player);
		}
		return null;
	}

}
