package io.github.danielm59.infinitetools.inventory;

import io.github.danielm59.infinitetools.inventory.slots.SlotOutput;
import io.github.danielm59.infinitetools.tile.TileIngotEnricher;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.IContainerListener;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ContainerIngotEnricher extends BaseContainer
	{
		public static final int INGOTENRICHER_INPUTS = 2;

		public static final int INGOTENRICHER_OUTPUTS = 1;

		private int lastProcessTime;

		private TileIngotEnricher tileIngotEnricher;

		public ContainerIngotEnricher(InventoryPlayer inventory, TileIngotEnricher tileIngotEnricher, EntityPlayer player)
		{
			this.tileIngotEnricher = tileIngotEnricher;
			tileIngotEnricher.openInventory(player);
			// Add the Input slots to the container
		for (int InputIndex = 0; InputIndex < INGOTENRICHER_INPUTS; ++InputIndex)
		{
			this.addSlotToContainer(new Slot(tileIngotEnricher, InputIndex, 56, 26 + InputIndex * 18));
		}
		// Add the Output slots to the container
		for (int OutputIndex = 0; OutputIndex < INGOTENRICHER_OUTPUTS; ++OutputIndex)
		{
			this.addSlotToContainer(new SlotOutput(tileIngotEnricher, INGOTENRICHER_INPUTS + OutputIndex, 116, 35 + OutputIndex * 18));
		}
		// Add the player's inventory slots to the container
		for (int inventoryRowIndex = 0; inventoryRowIndex < PLAYER_INVENTORY_ROWS; ++inventoryRowIndex)
		{
			for (int inventoryColumnIndex = 0; inventoryColumnIndex < PLAYER_INVENTORY_COLUMNS; ++inventoryColumnIndex)
			{
				this.addSlotToContainer(new Slot(inventory, inventoryColumnIndex + inventoryRowIndex * 9 + 9, 8 + inventoryColumnIndex * 18, 84 + inventoryRowIndex * 18));
			}
		}
		// Add the player's hot bar slots to the container
		for (int actionBarSlotIndex = 0; actionBarSlotIndex < PLAYER_INVENTORY_COLUMNS; ++actionBarSlotIndex)
		{
			this.addSlotToContainer(new Slot(inventory, actionBarSlotIndex, 8 + actionBarSlotIndex * 18, 142));
		}
	}

	@Override
	public void onContainerClosed(EntityPlayer entityPlayer)
	{
		super.onContainerClosed(entityPlayer);
		tileIngotEnricher.closeInventory(entityPlayer);
	}

	@Override
	public ItemStack transferStackInSlot(EntityPlayer entityPlayer, int slotIndex)
	{
		ItemStack newItemStack = null;
		Slot slot = inventorySlots.get(slotIndex);
		if (slot != null && slot.getHasStack())
		{
			ItemStack itemStack = slot.getStack();
			newItemStack = itemStack.copy();
			if (slotIndex < INGOTENRICHER_INPUTS + INGOTENRICHER_OUTPUTS)
			{
				if (!this.mergeItemStack(itemStack, INGOTENRICHER_INPUTS + INGOTENRICHER_OUTPUTS, inventorySlots.size(), false))
				{
					return null;
				}
			} else if (!this.mergeItemStack(itemStack, 0, INGOTENRICHER_INPUTS + INGOTENRICHER_OUTPUTS, false))
			{
				return null;
			}
			if (itemStack.stackSize == 0)
			{
				slot.putStack(null);
			} else
			{
				slot.onSlotChanged();
			}
		}
		return newItemStack;
	}

	@Override
	public void detectAndSendChanges()
	{
		super.detectAndSendChanges();
		for (IContainerListener crafter : this.listeners)
		{
			if (this.lastProcessTime != this.tileIngotEnricher.currentProcessTime)
			{
				crafter.sendProgressBarUpdate(this, 0, this.tileIngotEnricher.currentProcessTime);
			}
		}
		this.lastProcessTime = this.tileIngotEnricher.currentProcessTime;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void updateProgressBar(int valueType, int updatedValue)
	{
		if (valueType == 0)
		{
			this.tileIngotEnricher.currentProcessTime = updatedValue;
		}
	}
}
