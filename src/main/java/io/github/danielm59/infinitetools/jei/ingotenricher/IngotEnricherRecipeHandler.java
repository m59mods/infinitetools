package io.github.danielm59.infinitetools.jei.ingotenricher;

import javax.annotation.Nonnull;

import io.github.danielm59.infinitetools.jei.ITRecipeCategoryUid;
import io.github.danielm59.infinitetools.recipe.ingotenricher.IngotEnricherRecipe;
import mezz.jei.api.recipe.IRecipeHandler;
import mezz.jei.api.recipe.IRecipeWrapper;

public class IngotEnricherRecipeHandler implements IRecipeHandler<IngotEnricherRecipeWrapper>
{
	@Nonnull
	@Override
	public Class<IngotEnricherRecipeWrapper> getRecipeClass()
	{
		return IngotEnricherRecipeWrapper.class;
	}

	@Nonnull
	@Override
	public String getRecipeCategoryUid()
	{
		return ITRecipeCategoryUid.INGOTENRICHER;
	}

	@Nonnull
	@Override
	public String getRecipeCategoryUid(IngotEnricherRecipeWrapper recipe)
	{
		return ITRecipeCategoryUid.INGOTENRICHER;
	}

	@Nonnull
	@Override
	public IRecipeWrapper getRecipeWrapper(@Nonnull IngotEnricherRecipeWrapper recipe)
	{
		return recipe;
	}

	@Override
	public boolean isRecipeValid(@Nonnull IngotEnricherRecipeWrapper wrapper)
	{
		IngotEnricherRecipe recipe = wrapper.getRecipe();
		if (recipe.getInputTop() == null || recipe.getInputTop().stackSize <= 0)
		{
			return false;
		} else if (recipe.getInputBottom() == null || recipe.getInputBottom().stackSize <= 0)
		{
			return false;
		}
		return recipe.getOutput() != null;
	}
}