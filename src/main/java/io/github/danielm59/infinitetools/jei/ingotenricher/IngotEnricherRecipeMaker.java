package io.github.danielm59.infinitetools.jei.ingotenricher;

import java.util.ArrayList;
import java.util.List;

import io.github.danielm59.infinitetools.recipe.ingotenricher.IngotEnricherRecipe;
import io.github.danielm59.infinitetools.recipe.ingotenricher.IngotEnricherRegistry;

public class IngotEnricherRecipeMaker 
{
	private IngotEnricherRecipeMaker()
	{
	}

	public static List<IngotEnricherRecipeWrapper> getIngotEnricherRecipes()
	{
		List<IngotEnricherRecipeWrapper> recipes = new ArrayList<IngotEnricherRecipeWrapper>();
		for (IngotEnricherRecipe recipe : IngotEnricherRegistry.getAllRecipes())
		{
			recipes.add(new IngotEnricherRecipeWrapper(recipe));
		}
		return recipes;
	}
}
