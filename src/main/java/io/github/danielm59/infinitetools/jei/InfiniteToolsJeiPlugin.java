package io.github.danielm59.infinitetools.jei;

import javax.annotation.Nonnull;

import io.github.danielm59.infinitetools.block.ModBlocks;
import io.github.danielm59.infinitetools.gui.inventory.GuiIngotEnricher;
import io.github.danielm59.infinitetools.jei.ingotenricher.IngotEnricherRecipeCategory;
import io.github.danielm59.infinitetools.jei.ingotenricher.IngotEnricherRecipeHandler;
import io.github.danielm59.infinitetools.jei.ingotenricher.IngotEnricherRecipeMaker;
import mezz.jei.api.BlankModPlugin;
import mezz.jei.api.IGuiHelper;
import mezz.jei.api.IJeiHelpers;
import mezz.jei.api.IModRegistry;
import mezz.jei.api.JEIPlugin;
import net.minecraft.item.ItemStack;

@JEIPlugin
public class InfiniteToolsJeiPlugin extends BlankModPlugin
{
		@Override
		public void register(@Nonnull IModRegistry registry)
		{
			IJeiHelpers jeiHelpers = registry.getJeiHelpers();
			IGuiHelper guiHelper = jeiHelpers.getGuiHelper();	
			registry.addRecipeCategories(new IngotEnricherRecipeCategory(guiHelper));
			registry.addRecipeHandlers(new IngotEnricherRecipeHandler());
			registry.addRecipes(IngotEnricherRecipeMaker.getIngotEnricherRecipes());
			registry.addRecipeClickArea(GuiIngotEnricher.class, 80, 35, 22, 15, ITRecipeCategoryUid.INGOTENRICHER);
			registry.addRecipeCategoryCraftingItem(new ItemStack(ModBlocks.IngotEnricher), ITRecipeCategoryUid.INGOTENRICHER);
		}

}