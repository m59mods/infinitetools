package io.github.danielm59.infinitetools.jei;

import javax.annotation.Nonnull;

import mezz.jei.api.recipe.BlankRecipeWrapper;

public abstract class ITRecipeWrapper<R> extends BlankRecipeWrapper 
{
	@Nonnull
	private final R recipe;

	public ITRecipeWrapper(@Nonnull R recipe)
	{
		this.recipe = recipe;
	}

	@Nonnull
	public R getRecipe()
	{
		return recipe;
	}
}
