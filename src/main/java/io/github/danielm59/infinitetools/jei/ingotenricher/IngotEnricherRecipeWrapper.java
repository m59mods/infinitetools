package io.github.danielm59.infinitetools.jei.ingotenricher;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.Nonnull;

import io.github.danielm59.infinitetools.jei.ITRecipeWrapper;
import io.github.danielm59.infinitetools.recipe.ingotenricher.IngotEnricherRecipe;
import net.minecraft.item.ItemStack;

public class IngotEnricherRecipeWrapper extends ITRecipeWrapper<IngotEnricherRecipe>
{
	public IngotEnricherRecipeWrapper(IngotEnricherRecipe recipe)
	{
		super(recipe);
	}

	@Nonnull
	@Override
	public List<ItemStack> getInputs()
	{
		List<ItemStack> inputs = new ArrayList<ItemStack>();
		inputs.add(getRecipe().getInputTop());
		inputs.add(getRecipe().getInputBottom());
		return inputs;
	}

	@Nonnull
	@Override
	public List<ItemStack> getOutputs()
	{
		return Collections.singletonList(getRecipe().getOutput());
	}
}
