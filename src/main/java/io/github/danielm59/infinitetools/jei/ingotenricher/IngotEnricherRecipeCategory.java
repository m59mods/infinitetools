package io.github.danielm59.infinitetools.jei.ingotenricher;

import javax.annotation.Nonnull;

import io.github.danielm59.infinitetools.InfiniteTools;
import io.github.danielm59.infinitetools.block.ModBlocks;
import io.github.danielm59.infinitetools.jei.ITRecipeCategory;
import io.github.danielm59.infinitetools.jei.ITRecipeCategoryUid;
import mezz.jei.api.IGuiHelper;
import mezz.jei.api.gui.IDrawableAnimated;
import mezz.jei.api.gui.IDrawableStatic;
import mezz.jei.api.gui.IGuiItemStackGroup;
import mezz.jei.api.gui.IRecipeLayout;
import mezz.jei.api.recipe.IRecipeWrapper;
import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;

public class IngotEnricherRecipeCategory extends ITRecipeCategory
{
	private static final int inputTopSlot = 0;

	private static final int inputBottomSlot = 1;

	private static final int outputSlot = 2;

	private final static ResourceLocation guiTexture = new ResourceLocation(InfiniteTools.MODID.toLowerCase(), "textures/gui/2to1Gui.png");

	@Nonnull
	private final IDrawableAnimated arrow;

	public IngotEnricherRecipeCategory(IGuiHelper guiHelper)
	{
		super(guiHelper.createDrawable(guiTexture, 55, 16, 82, 54), ModBlocks.IngotEnricher.getUnlocalizedName() + ".name");
		IDrawableStatic arrowDrawable = guiHelper.createDrawable(guiTexture, 176, 0, 22, 15);
		this.arrow = guiHelper.createAnimatedDrawable(arrowDrawable, 200, IDrawableAnimated.StartDirection.LEFT, false);
	}

	@Override
	public String getUid()
	{
		return ITRecipeCategoryUid.INGOTENRICHER;
	}

	@Override
	public void drawAnimations(@Nonnull Minecraft minecraft)
	{
		arrow.draw(minecraft, 24, 18);
	}

	@Override
	public void setRecipe(IRecipeLayout recipeLayout, IRecipeWrapper recipeWrapper)
	{
		IGuiItemStackGroup guiItemStacks = recipeLayout.getItemStacks();
		guiItemStacks.init(inputTopSlot, true, 0, 9);
		guiItemStacks.init(inputBottomSlot, true, 0, 27);
		guiItemStacks.init(outputSlot, false, 60, 18);
		guiItemStacks.setFromRecipe(inputTopSlot, recipeWrapper.getInputs().get(inputTopSlot));
		guiItemStacks.setFromRecipe(inputBottomSlot, recipeWrapper.getInputs().get(inputBottomSlot));
		guiItemStacks.setFromRecipe(outputSlot, recipeWrapper.getOutputs());
	}
}