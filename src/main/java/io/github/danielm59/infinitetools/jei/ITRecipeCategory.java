package io.github.danielm59.infinitetools.jei;

import javax.annotation.Nonnull;

import mezz.jei.api.gui.IDrawable;
import mezz.jei.api.recipe.BlankRecipeCategory;
import mezz.jei.api.recipe.IRecipeWrapper;
import mezz.jei.util.Translator;

public abstract class ITRecipeCategory extends BlankRecipeCategory<IRecipeWrapper> 
{
	@Nonnull
	private final IDrawable background;

	@Nonnull
	private final String localizedName;

	public ITRecipeCategory(@Nonnull IDrawable background, String unlocalizedName)
	{
		this.background = background;
		this.localizedName = Translator.translateToLocal(unlocalizedName);
	}

	@Nonnull
	@Override
	public String getTitle()
	{
		return localizedName;
	}

	@Nonnull
	@Override
	public IDrawable getBackground()
	{
		return background;
	}
}
