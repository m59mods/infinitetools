package io.github.danielm59.infinitetools.recipes;

import io.github.danielm59.infinitetools.armor.Armor;
import io.github.danielm59.infinitetools.block.ModBlocks;
import io.github.danielm59.infinitetools.item.ModItems;
import io.github.danielm59.infinitetools.recipe.ingotenricher.IngotEnricherRegistry;
import io.github.danielm59.infinitetools.tool.Tools;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.oredict.ShapedOreRecipe;

public class Recipes 
{
	public static void init()
	{
		GameRegistry.addSmelting(ModBlocks.ScandiumOre, new ItemStack(ModItems.infIngot), 2.0F);
		
		IngotEnricherRegistry.addRecipe(new ItemStack(ModItems.infIngot), new ItemStack(Items.NETHER_STAR), new ItemStack(ModItems.infEnrichedIngot));
		IngotEnricherRegistry.addRecipe(new ItemStack(ModItems.infIngot), new ItemStack(Items.FLINT), new ItemStack(ModItems.infArrowHead));
		 
		GameRegistry.addRecipe(new ShapedOreRecipe(Armor.infHead, "III", "I I", 'I', ModItems.infEnrichedIngot ));
		GameRegistry.addRecipe(new ShapedOreRecipe(Armor.infChest, "I I", "III", "III", 'I', ModItems.infEnrichedIngot ));
		GameRegistry.addRecipe(new ShapedOreRecipe(Armor.infLegs, "III", "I I", "I I", 'I', ModItems.infEnrichedIngot ));
		GameRegistry.addRecipe(new ShapedOreRecipe(Armor.infBoots, "I I", "I I", 'I', ModItems.infEnrichedIngot ));

		GameRegistry.addRecipe(new ShapedOreRecipe(Tools.infpick, "III", " S ", " S ", 'I', ModItems.infEnrichedIngot, 'S', Items.STICK ));
		GameRegistry.addRecipe(new ShapedOreRecipe(Tools.infhoe, "II ", " S ", " S ", 'I', ModItems.infEnrichedIngot, 'S', Items.STICK ));
		GameRegistry.addRecipe(new ShapedOreRecipe(Tools.infhoe, " II", " S ", " S ", 'I', ModItems.infEnrichedIngot, 'S', Items.STICK ));
		GameRegistry.addRecipe(new ShapedOreRecipe(Tools.infshovel, " I ", " S ", " S ", 'I', ModItems.infEnrichedIngot, 'S', Items.STICK ));
		GameRegistry.addRecipe(new ShapedOreRecipe(Tools.infaxe, "II ", "IS ", " S ", 'I', ModItems.infEnrichedIngot, 'S', Items.STICK ));
		GameRegistry.addRecipe(new ShapedOreRecipe(Tools.infaxe, " II", " SI", " S ", 'I', ModItems.infEnrichedIngot, 'S', Items.STICK ));
		GameRegistry.addRecipe(new ShapedOreRecipe(Tools.infsword, " I ", " I ", " S ", 'I', ModItems.infEnrichedIngot, 'S', Items.STICK ));
		GameRegistry.addRecipe(new ShapedOreRecipe(Tools.infbow, " I ", "IBI", " I ", 'I', ModItems.infEnrichedIngot, 'B', Items.BOW ));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModItems.infArrow, 4), "  F", " S ", "C  ", 'F', ModItems.infArrowHead, 'S', Items.STICK, 'C', Items.FEATHER ));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModItems.infArrow, 4), "F  ", " S ", "  C", 'F', ModItems.infArrowHead, 'S', Items.STICK, 'C', Items.FEATHER ));
		GameRegistry.addRecipe(new ShapedOreRecipe(ModBlocks.IngotEnricher, "TPT", "T T", "TPT", 'T', ModItems.infIngot, 'P', Blocks.PISTON));
		
		
	}
}
