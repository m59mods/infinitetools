package io.github.danielm59.infinitetools;

import org.apache.logging.log4j.Logger;

import io.github.danielm59.infinitetools.proxy.CommonProxy;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = InfiniteTools.MODID, version = InfiniteTools.VERSION)
public class InfiniteTools
{
    public static final String MODID = "infinitetools";
    public static final String VERSION = "@VERSION@";
    public static final String ClientProxy = "io.github.danielm59.infinitetools.proxy.ClientProxy";
    public static final String ServerProxy = "io.github.danielm59.infinitetools.proxy.ServerProxy";
    
    @SidedProxy(clientSide = ClientProxy, serverSide = ServerProxy)
    public static CommonProxy proxy;

    @Mod.Instance
    public static InfiniteTools instance;

    public static Logger Logger;
    
    @EventHandler 
    public void preInit(FMLPreInitializationEvent event)
    {
    	Logger = event.getModLog();
    	proxy.preInit(event);
    	
    }
    
    @EventHandler
    public void init(FMLInitializationEvent event)
    {
    	proxy.init(event);
    }
    
    @EventHandler
    public void postInit(FMLPostInitializationEvent event)
    {
    	proxy.postInit(event);
    }
}
