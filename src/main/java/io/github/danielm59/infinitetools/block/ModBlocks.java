package io.github.danielm59.infinitetools.block;

import io.github.danielm59.infinitetools.InfiniteTools;
import io.github.danielm59.infinitetools.util.TextureHelper;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class ModBlocks {

	public static final Block ScandiumOre = new BlockScandiumOre();
	public static final Item ItemScandiumOre = new ItemBlock(ScandiumOre);
	public static final Block IngotEnricher = new BlockIngotEnricher();
	public static final Item ItemIngotEnricher = new ItemBlock(IngotEnricher);
	
	
	public static void init(){
		ScandiumOre.setRegistryName(InfiniteTools.MODID, "orescandium");
		ScandiumOre.setUnlocalizedName(ScandiumOre.getRegistryName().toString());
		GameRegistry.register(ScandiumOre);
		
		ItemScandiumOre.setRegistryName(ScandiumOre.getRegistryName());
		ItemScandiumOre.setUnlocalizedName(ItemScandiumOre.getRegistryName().toString());
		GameRegistry.register(ItemScandiumOre);
		
		IngotEnricher.setRegistryName(InfiniteTools.MODID, "ingotenricher");
		IngotEnricher.setUnlocalizedName(IngotEnricher.getRegistryName().toString());
		GameRegistry.register(IngotEnricher);
		
		ItemIngotEnricher.setRegistryName(IngotEnricher.getRegistryName());
		ItemIngotEnricher.setUnlocalizedName(ItemIngotEnricher.getRegistryName().toString());
		GameRegistry.register(ItemIngotEnricher);
	}	
	public static void inittextures(){
		
		TextureHelper.register(ItemScandiumOre);
		TextureHelper.register(ItemIngotEnricher);
	}
	
}
