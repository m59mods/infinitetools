package io.github.danielm59.infinitetools.block;

import io.github.danielm59.infinitetools.InfiniteTools;
import io.github.danielm59.infinitetools.gui.inventory.GuiID;
import io.github.danielm59.infinitetools.tile.TileIngotEnricher;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockIngotEnricher extends BlockMachine
{

	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta) 
	{
		return new TileIngotEnricher();
	}
	
	@Override
	public boolean onBlockActivated(World world, BlockPos p, IBlockState state, EntityPlayer player, EnumHand hand, ItemStack heldItem, EnumFacing side, float hitX, float hitY, float hitZ)
	{
		if (player.isSneaking())
		{
			return true;
		} else
		{
			if (!world.isRemote && world.getTileEntity(p) instanceof TileIngotEnricher)
			{
				player.openGui(InfiniteTools.instance, GuiID.INGOT_ENRICHER.ordinal(), world, p.getX(), p.getY(), p.getZ());
			}
			return true;
		}
	}
	
	@Override
	public boolean isOpaqueCube(IBlockState state)
	{
		return false;
	}

	@Override
	public boolean isFullCube(IBlockState state)
	{
		return false;
	}
}
