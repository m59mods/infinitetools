package io.github.danielm59.infinitetools.block;

import io.github.danielm59.infinitetools.creativetab.CreativeTabIF;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

public class BlockScandiumOre extends Block {

	public BlockScandiumOre() {
		super(Material.ROCK);
		this.setSoundType(SoundType.STONE);
	 	this.setCreativeTab(CreativeTabIF.INFINITETOOLS_TAB);
		this.setHardness(70.0F);
		this.setResistance(2000.0F);
		this.setHarvestLevel("pickaxe", 3);
	}

}
