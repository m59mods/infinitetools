package io.github.danielm59.infinitetools.recipe.ingotenricher;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.item.ItemStack;

public class IngotEnricherRegistry 
{
	private static IngotEnricherRegistry  INSTANCE = new IngotEnricherRegistry ();

	private final static List<IngotEnricherRecipe> IngotEnricherRecipes = new ArrayList<IngotEnricherRecipe>();

	private IngotEnricherRegistry()
	{
	}

	public static IngotEnricherRegistry getInstance()
	{
		return INSTANCE;
	}

	public static void addRecipe(IngotEnricherRecipe recipe)
	{
		IngotEnricherRecipes.add(recipe);
	}

	public static void addRecipe(ItemStack inputTop, ItemStack inputBottom, ItemStack output)
	{
		addRecipe(new IngotEnricherRecipe(inputTop, inputBottom, output));
	}

	public static List<IngotEnricherRecipe> getAllRecipes()
	{
		return IngotEnricherRecipes;
	}

	public IngotEnricherRecipe getMatchingRecipe(ItemStack inputTopSlot, ItemStack inputBottomSlot, ItemStack outputSlot)
	{
		for (IngotEnricherRecipe recipe : IngotEnricherRecipes)
		{
			if (inputTopSlot != null)
			{
				if (recipe.getInputTop().isItemEqual(inputTopSlot))
				{
					if (inputBottomSlot != null)
					{
						if (recipe.getInputBottom().isItemEqual(inputBottomSlot))
						{
							if (outputSlot != null)
							{
								ItemStack craftingResult = recipe.getOutput();
								if (!ItemStack.areItemStackTagsEqual(outputSlot, craftingResult) || !outputSlot.isItemEqual(craftingResult))
								{
									continue;
								} else if (craftingResult.stackSize + outputSlot.stackSize > outputSlot.getMaxStackSize())
								{
									continue;
								}
							}
							return recipe;
						}
					}
				}
			}
		}
		return null;
	}
}