package io.github.danielm59.infinitetools.recipe.ingotenricher;

import net.minecraft.item.ItemStack;

public class IngotEnricherRecipe 
		{
		private ItemStack inputTop;

		private ItemStack inputBottom;

		private ItemStack output;

		IngotEnricherRecipe(ItemStack inputTop, ItemStack inputBottom, ItemStack output)
		{
			this.inputTop = inputTop;
			this.inputBottom = inputBottom;
			this.output = output;
		}

		public ItemStack getInputTop()
		{
			return inputTop;
		}

		public ItemStack getInputBottom()
		{
			return inputBottom;
		}

		public ItemStack getOutput()
		{
			return output;
		}
	}