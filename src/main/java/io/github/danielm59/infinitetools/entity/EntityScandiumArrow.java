package io.github.danielm59.infinitetools.entity;

import io.github.danielm59.infinitetools.item.ModItems;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class EntityScandiumArrow extends EntityArrow{

	public EntityScandiumArrow(World worldIn) {
		super(worldIn);
	}
	
	 public EntityScandiumArrow(World worldIn, EntityLivingBase shooter)
	    {
		 super(worldIn, shooter);	
		 setDamage(30);
	    }

	@Override
	protected ItemStack getArrowStack() {
	    return new ItemStack(ModItems.infArrow);	
	}

}

