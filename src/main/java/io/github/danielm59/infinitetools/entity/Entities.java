package io.github.danielm59.infinitetools.entity;

import io.github.danielm59.infinitetools.InfiniteTools;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.registry.EntityRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class Entities {
	public static void init()
	{
		EntityRegistry.registerModEntity(EntityScandiumArrow.class, "Scandium Arrow", 0, InfiniteTools.instance, 32, 5, true);
	}
	
	@SideOnly(Side.CLIENT)
	public static void initModels() {
		RenderingRegistry.registerEntityRenderingHandler(EntityScandiumArrow.class, RenderScandiumArrow.FACTORY);
	}
}
