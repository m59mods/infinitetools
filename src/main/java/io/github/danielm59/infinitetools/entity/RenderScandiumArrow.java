package io.github.danielm59.infinitetools.entity;

import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderArrow;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.client.registry.IRenderFactory;

public class RenderScandiumArrow extends RenderArrow<EntityScandiumArrow>{

	private ResourceLocation arrowTexture = new ResourceLocation("infinitetools:textures/entities/infArrow.png");
	
	public static final Factory FACTORY = new Factory();
	
	public RenderScandiumArrow(RenderManager renderManagerIn) {
		super(renderManagerIn);
	}

	@Override
	protected ResourceLocation getEntityTexture(EntityScandiumArrow entity) {
		return arrowTexture;
	}
	
	public static class Factory implements IRenderFactory<EntityScandiumArrow> {

		@Override
		public Render<? super EntityScandiumArrow> createRenderFor(RenderManager manager) {
			return new RenderScandiumArrow(manager);
		}

	}

}
