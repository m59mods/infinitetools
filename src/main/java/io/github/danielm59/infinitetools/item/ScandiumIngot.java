package io.github.danielm59.infinitetools.item;

import io.github.danielm59.infinitetools.creativetab.CreativeTabIF;
import net.minecraft.item.Item;

public class ScandiumIngot extends Item {

	public ScandiumIngot() {
		super();
	 	this.setCreativeTab(CreativeTabIF.INFINITETOOLS_TAB);
	}

}
