package io.github.danielm59.infinitetools.item;

import io.github.danielm59.infinitetools.creativetab.CreativeTabIF;
import io.github.danielm59.infinitetools.entity.EntityScandiumArrow;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.entity.projectile.EntityTippedArrow;
import net.minecraft.item.ItemArrow;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ScandiumArrow extends ItemArrow { 
	
	    public ScandiumArrow()
	    {
		 	this.setCreativeTab(CreativeTabIF.INFINITETOOLS_TAB);
	    }
	    
	    @Override
	    
	    public EntityArrow createArrow(World worldIn, ItemStack stack, EntityLivingBase shooter)
	    {
	         return new EntityScandiumArrow(worldIn, shooter);
	    }

	    public boolean isInfinite(ItemStack stack, ItemStack bow, net.minecraft.entity.player.EntityPlayer player)
	    {
	        int enchant = net.minecraft.enchantment.EnchantmentHelper.getEnchantmentLevel(net.minecraft.init.Enchantments.INFINITY, bow);
	        return enchant <= 0 ? false : this.getClass() == ScandiumArrow.class;
	    }
	}
