package io.github.danielm59.infinitetools.item;

import io.github.danielm59.infinitetools.creativetab.CreativeTabIF;
import net.minecraft.item.Item;

public class ScandiumArrowHead extends Item
{
	public ScandiumArrowHead() 
	{
		super();
	 	this.setCreativeTab(CreativeTabIF.INFINITETOOLS_TAB);
	}
}
