package io.github.danielm59.infinitetools.item;

import io.github.danielm59.infinitetools.InfiniteTools;
import io.github.danielm59.infinitetools.util.TextureHelper;
import net.minecraft.item.Item;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class ModItems {
	public static final Item infArrow = new ScandiumArrow();
	public static final Item infIngot = new ScandiumIngot();
	public static final Item infEnrichedIngot = new EnrichedScandiumIngot();
	public static final Item infArrowHead = new ScandiumArrowHead();

	
	public static void init()
	{
		infArrow.setRegistryName(InfiniteTools.MODID, "infarrow");
		infArrow.setUnlocalizedName(infArrow.getRegistryName().toString());
		GameRegistry.register(infArrow);
		
		infIngot.setRegistryName(InfiniteTools.MODID, "ingot_scandium");
		infIngot.setUnlocalizedName(infIngot.getRegistryName().toString());
		GameRegistry.register(infIngot);
		
		infEnrichedIngot.setRegistryName(InfiniteTools.MODID, "infenrichedingot");
		infEnrichedIngot.setUnlocalizedName(infEnrichedIngot.getRegistryName().toString());
		GameRegistry.register(infEnrichedIngot);
		
		infArrowHead.setRegistryName(InfiniteTools.MODID, "infarrowhead");
		infArrowHead.setUnlocalizedName(infArrowHead.getRegistryName().toString());
		GameRegistry.register(infArrowHead);
			
		
	}
	
	public static void inittextures()
	{
		
		TextureHelper.register(infArrow);
		TextureHelper.register(infIngot);
		TextureHelper.register(infEnrichedIngot);
		TextureHelper.register(infArrowHead);
	}
	
}
