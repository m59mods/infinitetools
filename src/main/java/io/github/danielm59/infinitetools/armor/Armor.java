package io.github.danielm59.infinitetools.armor;

import io.github.danielm59.infinitetools.InfiniteTools;
import io.github.danielm59.infinitetools.util.TextureHelper;
import net.minecraft.item.Item;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class Armor 
{

	public static final Item infHead = new ScandiumArmorHelm();
	public static final Item infChest = new ScandiumArmorChest();
	public static final Item infLegs = new ScandiumArmorLeggins();
	public static final Item infBoots = new ScandiumArmorBoots();

	public static void init() 
	{
		infHead.setRegistryName(InfiniteTools.MODID, "infhead");
		infHead.setUnlocalizedName(infHead.getRegistryName().toString());
		GameRegistry.register(infHead);

		infChest.setRegistryName(InfiniteTools.MODID, "infchest");
		infChest.setUnlocalizedName(infChest.getRegistryName().toString());
		GameRegistry.register(infChest);

		infLegs.setRegistryName(InfiniteTools.MODID, "inflegs");
		infLegs.setUnlocalizedName(infLegs.getRegistryName().toString());
		GameRegistry.register(infLegs);

		infBoots.setRegistryName(InfiniteTools.MODID, "infboots");
		infBoots.setUnlocalizedName(infBoots.getRegistryName().toString());
		GameRegistry.register(infBoots);
	}

	public static void inittextures() 
	{
		TextureHelper.register(infHead);
		TextureHelper.register(infChest);
		TextureHelper.register(infLegs);
		TextureHelper.register(infBoots);
	}
}