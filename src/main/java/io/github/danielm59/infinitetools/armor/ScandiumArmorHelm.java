package io.github.danielm59.infinitetools.armor;

import io.github.danielm59.infinitetools.creativetab.CreativeTabIF;
import io.github.danielm59.infinitetools.tool.Materials;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ScandiumArmorHelm extends ItemArmor 
{

	public ScandiumArmorHelm() {
		super(Materials.ScandiumArmor, 0, EntityEquipmentSlot.HEAD);
		setMaxStackSize(1);
		setMaxDamage(0);
	 	this.setCreativeTab(CreativeTabIF.INFINITETOOLS_TAB);
	}

}
