package io.github.danielm59.infinitetools.tool;

import io.github.danielm59.infinitetools.InfiniteTools;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraft.item.ItemArmor.ArmorMaterial;
import net.minecraftforge.common.util.EnumHelper;

public class Materials {
	public static ToolMaterial ScandiumPick = EnumHelper.addToolMaterial("ScandiumPick", 10, Integer.MAX_VALUE, 60.0F, 3.0F, 22);
	public static ToolMaterial ScandiumSword = EnumHelper.addToolMaterial("ScandiumSword", 10, Integer.MAX_VALUE, 60.0F, 999996.0F, 22);
	public static ToolMaterial Scandium = EnumHelper.addToolMaterial("Scandium", 10, Integer.MAX_VALUE, 60.0F, 3.0F, 22);
	public static ToolMaterial ScandiumAxe = EnumHelper.addToolMaterial("ScandiumAxe", 10, Integer.MAX_VALUE, 60.0F, 27.0F, 22);
	public static ArmorMaterial ScandiumArmor = EnumHelper.addArmorMaterial("ScandiumArmor", InfiniteTools.MODID+":"+ "scandium", Integer.MAX_VALUE, new int[] {8, 8, 8, 8}, 22, SoundEvents.ITEM_ARMOR_EQUIP_IRON, 0);
}

