package io.github.danielm59.infinitetools.tool;

import io.github.danielm59.infinitetools.creativetab.CreativeTabIF;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemPickaxe;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class ToolPickaxe extends ItemPickaxe 
{

	protected ToolPickaxe() 
	{
		super(Materials.ScandiumPick);
	 	this.setCreativeTab(CreativeTabIF.INFINITETOOLS_TAB);
	}
	
	@Override
	public boolean onBlockDestroyed(ItemStack stack, World worldIn, IBlockState state, BlockPos pos, EntityLivingBase entityLiving){
		return true;
	}
	
	@Override
	public boolean hitEntity(ItemStack stack, EntityLivingBase target, EntityLivingBase attacker)
    {
        return true;
    }
}

	
