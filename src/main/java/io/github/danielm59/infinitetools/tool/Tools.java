package io.github.danielm59.infinitetools.tool;

import io.github.danielm59.infinitetools.InfiniteTools;
import io.github.danielm59.infinitetools.util.TextureHelper;
import net.minecraft.item.Item;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class Tools {

	public static final Item infpick = new ToolPickaxe();
	public static final Item infsword = new ToolSword();
	public static final Item infshovel = new ToolShovel();
	public static final Item infhoe = new ToolHoe();
	public static final Item infaxe = new ToolAxe();
	public static final Item infbow = new ToolBow();
	
	public static void init(){
		infpick.setRegistryName(InfiniteTools.MODID, "infpick");
		infpick.setUnlocalizedName(infpick.getRegistryName().toString());
		GameRegistry.register(infpick);
		
		infsword.setRegistryName(InfiniteTools.MODID, "infsword");
		infsword.setUnlocalizedName(infsword.getRegistryName().toString());
		GameRegistry.register(infsword);
		
		infshovel.setRegistryName(InfiniteTools.MODID, "infshovel");
		infshovel.setUnlocalizedName(infshovel.getRegistryName().toString());
		GameRegistry.register(infshovel);
		
		infhoe.setRegistryName(InfiniteTools.MODID, "infhoe");
		infhoe.setUnlocalizedName(infhoe.getRegistryName().toString());
		GameRegistry.register(infhoe);
		
		infaxe.setRegistryName(InfiniteTools.MODID, "infaxe");
		infaxe.setUnlocalizedName(infaxe.getRegistryName().toString());
		GameRegistry.register(infaxe);
		
		infbow.setRegistryName(InfiniteTools.MODID, "infbow");
		infbow.setUnlocalizedName(infbow.getRegistryName().toString());
		GameRegistry.register(infbow);
	}
	public static void inittextures(){
		
		TextureHelper.register(infpick);
		TextureHelper.register(infsword);
		TextureHelper.register(infshovel);
		TextureHelper.register(infhoe);
		TextureHelper.register(infaxe);
		TextureHelper.register(infbow);
	}
	
}
