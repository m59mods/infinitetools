package io.github.danielm59.infinitetools.creativetab;

import io.github.danielm59.infinitetools.InfiniteTools;
import io.github.danielm59.infinitetools.block.ModBlocks;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class CreativeTabIF {
	public static final CreativeTabs INFINITETOOLS_TAB = new CreativeTabs(InfiniteTools.MODID)
	{
		@Override
		public Item getTabIconItem() 
		{
			return Item.getItemFromBlock(ModBlocks.ScandiumOre);
		}
	};
	
}
