package io.github.danielm59.infinitetools.proxy;

import io.github.danielm59.infinitetools.armor.Armor;
import io.github.danielm59.infinitetools.block.ModBlocks;
import io.github.danielm59.infinitetools.entity.Entities;
import io.github.danielm59.infinitetools.item.ModItems;
import io.github.danielm59.infinitetools.tool.Tools;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

public class ClientProxy extends CommonProxy {
	
	@Override
	public void preInit(FMLPreInitializationEvent e) {
		super.preInit(e);
		Entities.initModels();
	}
	
	@Override	
	public void init(FMLInitializationEvent e) {
		super.init(e);
		Tools.inittextures();
		ModBlocks.inittextures();
		Armor.inittextures();
		ModItems.inittextures();
		
	}
}
