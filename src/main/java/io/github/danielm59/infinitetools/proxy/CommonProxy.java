package io.github.danielm59.infinitetools.proxy;

import io.github.danielm59.infinitetools.InfiniteTools;
import io.github.danielm59.infinitetools.armor.Armor;
import io.github.danielm59.infinitetools.block.ModBlocks;
import io.github.danielm59.infinitetools.entity.Entities;
import io.github.danielm59.infinitetools.handler.EventHandler;
import io.github.danielm59.infinitetools.handler.GuiHandler;
import io.github.danielm59.infinitetools.item.ModItems;
import io.github.danielm59.infinitetools.recipes.Recipes;
import io.github.danielm59.infinitetools.tile.TileIngotEnricher;
import io.github.danielm59.infinitetools.tool.Tools;
import io.github.danielm59.infinitetools.world.WorldGenScandiumOre;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class CommonProxy {
	public void preInit(FMLPreInitializationEvent e)
	{
		Tools.init();
		ModBlocks.init();
		Armor.init();
		GameRegistry.registerWorldGenerator(new WorldGenScandiumOre(), 0);
		ModItems.init();
		Entities.init();	
	}
	
	public void init(FMLInitializationEvent e) 
	{
		MinecraftForge.EVENT_BUS.register(new EventHandler());
		NetworkRegistry.INSTANCE.registerGuiHandler(InfiniteTools.instance, new GuiHandler());
		GameRegistry.registerTileEntityWithAlternatives(TileIngotEnricher.class, "ingotenricher", "tile.ingotenricher");
		Recipes.init();
	}
	
	public void postInit(FMLPostInitializationEvent e)
	{
		
	}
}
